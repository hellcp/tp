function repeat(func, times) {
	func();
	times && --times && repeat(func, times);
}

const scrollContainer = document.getElementById("timeline");

scrollContainer.addEventListener("wheel", (evt) => {
	evt.preventDefault();
	scrollContainer.scrollLeft += evt.deltaY;
	scrollContainer.scrollLeft += evt.deltaX;
});

var startTime = new Date(1628314200*1000);

var string = new Intl.DateTimeFormat('en-GB', { timeStyle: 'long' });
var timezone = string.format(startTime).split(' ')[1];

document.getElementById("tz-info").innerHTML = `All times shown are ${timezone} timezone`;

var delimiters = document.getElementById("delimiters");
var hours = document.getElementById("hours");
var minutes = document.getElementById("minutes");
var day = 0;
var hour = 0;
var span = 0;

var scrolled = false;

function set_time() {
	var now = new Date();
	if (now < startTime || now > new Date(startTime.getTime() + 24*60*60*1000))
		document.getElementById("overlay").style.visibility = "hidden";
	else {
		var sinceStart = (now - startTime) / (60*1000);
		document.getElementById("overlay").style.left = `calc(1rem + ${8 * sinceStart}px)`;
		document.getElementById("overlay").style.visibility = "visible";
		if (!scrolled)
			follow_indicator();
			var times = Object.keys(timings);
			var time = startTime.getTime();
			var now = new Date();
			Array.from(times).forEach(function(element) {
				var eventTime = new Date(Number(element));
				if (now >= eventTime)
					time = eventTime.getTime();
			});
			if (time in timings)
				display_info(timings[time]);
	}
}

function get_center() {
	var timeline = document.getElementById("timeline");
	var overlay = document.getElementById("overlay");
	if ((timeline.offsetWidth + timeline.scrollLeft >= timeline.scrollWidth && window.innerWidth / 2 <= overlay.getBoundingClientRect().left) || (timeline.scrollLeft == 0 && window.innerWidth / 2 >= overlay.getBoundingClientRect().left)) {
		return 0;
	}
	return overlay.getBoundingClientRect().left - (window.innerWidth>>1) + 1.5;
}

function follow_indicator() {
	document.getElementById("timeline").scrollBy(get_center(), 0);
	scrolled = false;
}

var latestTime = startTime;

repeat(function () {
	var col = document.createElement("col");
	if (hour != latestTime.getHours()) {
		var hourd = document.createElement("th");
		hourd.setAttribute("colspan", (60 - latestTime.getMinutes()) / 5);
		hourd.setAttribute("class", "hour");
		hourd.innerHTML = latestTime.getHours();
		hours.appendChild(hourd);
		col.setAttribute("class", "hour");
		hour = latestTime.getHours();
	}
	if (day != latestTime.getDate()) {
		col.setAttribute("class", "day");
		day = latestTime.getDate();
	}
	col.setAttribute("width", "40");
	delimiters.appendChild(col);

	var minute = document.createElement("th");
	minute.setAttribute("class", "minute");
	minute.innerHTML = latestTime.getMinutes() < 9 ? "0" + latestTime.getMinutes() : latestTime.getMinutes();
	minutes.appendChild(minute);
	latestTime = new Date(latestTime.getTime() + 5*60000);
}, 288);

function display_info(info, focus=true) {
	var elements = document.getElementsByClassName("highlighted");
	Array.from(elements).forEach(function(element) {
		element.classList.remove("highlighted");
	});
	if (info["startTime"] && focus)
		document.getElementById(`event-${new Date(info["startTime"]).getTime()}`).classList.add("highlighted");
	var container = document.getElementById("info");
	container.innerHTML = "";
	if (info["performer"]) {
		var performer = document.createElement("p");
		performer.setAttribute("class", "performer");
		var performerName = document.createElement("span");
		performerName.innerHTML = info["performer"];
		performer.appendChild(performerName);
		if (info["website"]) {
			var performerWebsite = document.createElement("span");
			performerWebsite.setAttribute("class", "website");
			performerWebsite.setAttribute("title", "Website");
			
			performerWebsite.innerHTML = `<a href="${info["website"]}" target="_blank"><svg width="24" height="24" fill="currentColor" version="1.1" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"> <path d="m11.484 2.0137c-5.1327 0.2585-9.2221 4.3515-9.4727 9.4863h3.0156a7 10 0 0 1 6.4727-9.4531v-0.033203c-0.005178 2.528e-4 -0.010449-2.607e-4 -0.015625 0zm1.0156 0v0.013672a7 10 0 0 1 6.4746 9.4727h3.0137c-0.25104-5.1397-4.3484-9.2353-9.4883-9.4863zm0 1.0195v8.4668h5.4746a6 9 0 0 0-5.4746-8.4668zm-1 0.021484a6 9 0 0 0-5.4746 8.4453h5.4746v-8.4453zm-9.4883 9.4453c0.25104 5.1397 4.3484 9.2353 9.4883 9.4863v-0.033203a7 10 0 0 1-6.4727-9.4531h-3.0156zm4.0137 0a6 9 0 0 0 5.4746 8.4453v-8.4453h-5.4746zm6.4746 0v8.4453a6 9 0 0 0 5.4746-8.4453h-5.4746zm6.4727 0a7 10 0 0 1-6.4727 9.4531v0.033203c5.1398-0.25101 9.2372-4.3466 9.4883-9.4863h-3.0156z"/></svg></a>`;
			performer.appendChild(performerWebsite);
		}
		if (info["youtube"]) {
			var performerYoutube = document.createElement("span");
			performerYoutube.setAttribute("class", "youtube");
			performerYoutube.setAttribute("title", "YouTube");
			
			performerYoutube.innerHTML = `<a href="${info["youtube"]}" target="_blank"><svg width="24" height="24" version="1.1" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" fill="currentColor"> <path d="m11.988 3.596s-7.4958 0.015152-9.3582 0.52309a3.0168 3.0168 0 0 0-2.1242 2.1242c-0.56335 3.3093-0.78198 8.3518 0.015332 11.529a3.0168 3.0168 0 0 0 2.1242 2.1239c1.8624 0.50794 9.3582 0.50806 9.3582 0.50806s7.4961-1.2e-4 9.3585-0.50806a3.0168 3.0168 0 0 0 2.1242-2.1239c0.59413-3.3139 0.77706-8.3533-0.015632-11.544a3.0168 3.0168 0 0 0-2.1239-2.1242c-1.8624-0.50794-9.3585-0.50776-9.3585-0.50776zm-2.3858 4.8022 6.2185 3.6018-6.2185 3.6018z"/></svg></a>`;
			performer.appendChild(performerYoutube);
		}
		if (info["soundcloud"]) {
			var performerSoundcloud = document.createElement("span");
			performerSoundcloud.setAttribute("class", "soundcloud");
			performerSoundcloud.setAttribute("title", "SoundCloud");
			
			performerSoundcloud.innerHTML = `<a href="${info["soundcloud"]}" target="_blank"><svg width="24" height="24" version="1.1" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" fill="currentColor"> <path d="m12 2c-5.54 0-10 4.46-10 10s4.46 10 10 10 10-4.46 10-10-4.46-10-10-10zm1.3765 6.7891c1.4583-0.054032 2.784 0.86603 3.2421 2.2892 0.08068 0.25064 0.10334 0.28362 0.18373 0.26748 1.1245-0.2258 2.1621 0.46998 2.3611 1.5835 0.16446 0.92027-0.42476 1.8576-1.369 2.178-0.2568 0.08711-0.34432 0.09023-2.9131 0.10271-2.4695 0.01199-2.6583 0.0077-2.7966-0.06248-0.10038-0.05101-0.16464-0.12332-0.19887-0.2237-0.07937-0.23275-0.07625-5.3765 0.0035-5.5671 0.11136-0.2665 0.53807-0.46079 1.1941-0.54365 0.09818-0.0124 0.196-0.020314 0.29322-0.023917zm-2.3223 0.89438c0.02265-0.00234 0.04789 0.00184 0.07555 0.012416 0.09166 0.035175 0.09726 0.0742 0.17396 1.2025 0.09897 1.4559 0.05444 3.5538-0.0888 4.1844-0.02666 0.11738-0.20992 0.13191-0.2674 0.02118-0.18298-0.35248-0.20001-4.0623-0.024-5.2297 0.01796-0.11909 0.06274-0.18367 0.13068-0.19059zm-1.986 0.49697c0.046041-0.0041 0.094355 0.01217 0.12563 0.05049 0.14492 0.17757 0.21973 3.4782 0.099646 4.3962-0.032009 0.2447-0.075582 0.47828-0.096827 0.51907-0.042229 0.08106-0.10956 0.0932-0.19986 0.036-0.10571-0.06696-0.18345-0.96182-0.18994-2.1865-0.00599-1.1301 0.073844-2.598 0.14922-2.7436 0.022304-0.04309 0.066098-0.06756 0.11214-0.07167zm0.99427 0.16834c0.06179-0.0097 0.12518 0.01876 0.15269 0.0912 0.14799 0.38967 0.19795 2.9966 0.07795 4.0683-0.03574 0.31918-0.07743 0.60044-0.09261 0.625-0.03477 0.05627-0.22082 0.05865-0.25506 0.0033-0.1676-0.27119-0.18886-4.0372-0.026234-4.6501 0.02125-0.08008 0.081466-0.12794 0.14325-0.13763zm-2.0011 0.27774c0.025706-0.0046 0.054007 0.0023 0.085076 0.01886 0.041496 0.02221 0.086276 0.09296 0.099473 0.15725 0.17015 0.82857 0.16876 3.2304-0.0024 4.1964-0.013533 0.07637-0.057888 0.15666-0.098564 0.17843-0.2481 0.13278-0.31893-0.37209-0.31805-2.2665 7.754e-4 -1.6639 0.054513-2.2525 0.23445-2.2845zm-1.9682 0.90737c0.18275 0 0.29193 0.63867 0.2861 1.6737-0.00446 0.79319-0.10736 1.7527-0.19383 1.8075-0.24887 0.15793-0.32244-0.17085-0.35719-1.5957-0.030357-1.2446 0.059686-1.8856 0.26491-1.8856zm0.98364 0.17197c0.046186-4e-3 0.094691 0.01243 0.12662 0.0509 0.077522 0.09341 0.17794 0.93418 0.17794 1.4899 0 0.47185-0.089386 1.5685-0.14963 1.8359-0.026452 0.11739-0.20989 0.13195-0.2669 0.02118-0.071698-0.13931-0.15549-1.1748-0.15344-1.8962 0.00193-0.6774 0.064265-1.2615 0.15269-1.4301 0.022658-0.04321 0.066533-0.06766 0.11272-0.07166zm-1.9805 0.38549c0.23557 0.0524 0.37231 0.97609 0.23106 1.8219-0.040274 0.24115-0.091036 0.47183-0.1128 0.51261-0.045986 0.08617-0.17696 0.09784-0.23768 0.02118-0.073266-0.09251-0.15782-0.75666-0.15782-1.2394-1.9e-6 -0.50374 0.079316-1.0067 0.17074-1.0825 0.037165-0.03084 0.072858-0.04116 0.10651-0.03368z"/></svg></a>`;
			performer.appendChild(performerSoundcloud);
		}
		if (info["bandcamp"]) {
			var performerBandcamp = document.createElement("span");
			performerBandcamp.setAttribute("class", "bandcamp");
			performerBandcamp.setAttribute("title", "Bandcamp");
			
			performerBandcamp.innerHTML = `<a href="${info["bandcamp"]}" target="_blank"><svg width="24" height="24" version="1.1" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" fill="currentColor"> <path d="m16.36 19.103h-16.36l7.6399-14.103h16.36l-7.64 14.103"/></svg></a>`;
			performer.appendChild(performerBandcamp);
		}
		container.appendChild(performer);
	}
	if (info["title"]) {
		var title = document.createElement("h2");
		title.setAttribute("class", "title");
		title.innerHTML = info["title"];
		container.appendChild(title);
	}
	if (info["startTime"] && info["endTime"]) {
		var time = document.createElement("p");
		time.setAttribute("class", "time");
		var start = new Date(info["startTime"]);
		var end = new Date(info["endTime"]);
		var string = new Intl.DateTimeFormat('en-GB', { timeStyle: 'short' });
		time.innerHTML = `${string.format(start)} - ${string.format(end)}`;
		container.appendChild(time);
	}
	if (info["description"]) {
		var description = document.createElement("p");
		description.setAttribute("class", "description");
		description.innerHTML = info["description"];
		container.appendChild(description);
	}
}

var room1 = document.getElementById("room1");

var timings = {};
latestTime = startTime;

Promise.all([
	fetch('schedule.json')
	.then(res => res.json())
])
.then(([schedule]) => {
	Array.from(schedule).forEach(function(element) {
		var event = document.createElement("td");
		event.setAttribute("class", "event");
		event.setAttribute("onclick", `display_info(timings[${latestTime.getTime()}])`);
		event.setAttribute("onkeyup", `display_info(timings[${latestTime.getTime()}])`);
		event.setAttribute("id", `event-${latestTime.getTime()}`);
		event.setAttribute("tabindex", 0);
		event.setAttribute("colspan", element["duration"] / 5);
		var eventContents = document.createElement("div");
		if (element["performer"]) {
			var performer = document.createElement("p");
			performer.setAttribute("class", "performer");
			performer.innerHTML = element["performer"];
			eventContents.appendChild(performer);
		}
		if (element["categories"]) {
			var categories = document.createElement("ul");
			categories.setAttribute("class", "categories");
			Array.from(element["categories"]).forEach(function(element) {
				var category = document.createElement("li");
				category.innerHTML = element;
				categories.appendChild(category);
			});
			eventContents.appendChild(categories);
		}
		if (element["title"]) {
			var title = document.createElement("p");
			title.setAttribute("class", "title");
			title.innerHTML = element["title"];
			eventContents.appendChild(title);
		}
		event.appendChild(eventContents);
		room1.appendChild(event);
		endTime = new Date(latestTime.getTime() + element["duration"]*60000);
		element["startTime"] = latestTime;
		element["endTime"] = endTime;
		timings[latestTime.getTime()] = element;
		var now = new Date();
		if (!scrolled) {
			var eventTime = new Date(latestTime);
			if (now >= eventTime)
				display_info(element);
		}
		latestTime = endTime;
	});
}).catch(err => {
	console.log(err)
});

set_time();
setInterval(set_time, 7.5*1000);

document.getElementById("timeline").addEventListener("scroll", function(){ 
	var center = get_center();
	scrolled = center < -1 || center > 1;
});
